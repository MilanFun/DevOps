DROP DATABASE IF EXISTS `employees`;
CREATE DATABASE IF NOT EXISTS `employees`;
USE `employees`;
CREATE TABLE IF NOT EXISTS `user` (id int, first_name VARCHAR(20), last_name VARCHAR(20), email VARCHAR(50), age int);