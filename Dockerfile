FROM maven:3.6.3-jdk-11-openj9
COPY . .
RUN mvn clean install -Dmaven.test.skip=true
CMD mvn spring-boot:run