# DevOps
DevOps project using Spring Boot and OpenAPI 3.0
Clone this repository.
```
git clone https://github.com/MilanFun/DevOps.git
```

To start the project, you need execute
```
docker-compose up
```

Go to:
```
http://localhost:9099/swagger-ui-custom.html
```
There are some documentation using OpenApi 3.0 (Swagger API)